from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    @api.depends('commission_inv_ids')
    def _count_commission(self):
        for order in self:
            order.commission_count = len(order.commission_inv_ids)
            
    @api.depends('team_id', 'project_project_id', 'commission_percent', 'commission_base')
    def _get_commission(self):
        for order in self:
            if order.project_project_id:
                if order.commission_type == 'percent':
                    order.commission_amount = order.commission_base * order.commission_percent / 100
                elif order.commission_type == 'fixed':
                    order.commission_amount = order.project_project_id.commission_amount
                    
    @api.depends('project_project_id.commission_percent')
    def _get_commission_percent(self):
        for order in self:
            if order.commission_type == 'percent':
                order.commission_percent = order.project_project_id.commission_percent

    bf_voucher_id = fields.Many2one('account.voucher', string='Booking Fee', readonly=True, copy=False)
    commission_inv_ids = fields.One2many('account.invoice', 'commission_sale_id', string='Commission', readonly=True, copy=False)
    commission_count = fields.Integer(string='# of Commission', compute='_count_commission', readonly=True)
    project_project_id = fields.Many2one('project.project', string='Project', required=True,
                                         domain=[('wbs_type', '=', 'phase')],
                                         readonly=True, states={'draft': [('readonly', False)]})
    project_id = fields.Many2one('account.analytic.account', 'Analytic Account',
                                help="The analytic account related to a sales order.",
                                related='project_project_id.analytic_account_id', store=True, readonly=True)
    state = fields.Selection(selection_add=[('draft', 'Booked'),
                                            ('installment', 'Installment'),
                                            ('agreement', 'Bank Agreement'),
                                            ('settled', 'Settled'),
                                            ('handover', 'Handover'), ])
    buyout_term = fields.Selection([('kpr', 'KPR'),
                                    ('cash', 'Cash')], string='Term', required=True, default='kpr',
                                   readonly=True, states={'draft': [('readonly', False)]})
    dp_kpr = fields.Float(string='DP KPR',
                          readonly=True, states={'draft': [('readonly', False)]})
    bank_partner_id = fields.Many2one('res.partner', string='Bank KPR',
                                      readonly=True, states={'draft': [('readonly', False)]})
    commission_type = fields.Selection([('percent', 'Percent'),
                                        ('fixed', 'Fixed')], string='Commission Type', readonly=True,
                                       related='team_id.commission_type', store=True)
    commission_amount = fields.Float(string='Commission', compute='_get_commission', store=True, readonly=False)
    commission_percent = fields.Float(string='Commission (%)', compute='_get_commission_percent', store=True, readonly=False)
    commission_base = fields.Float(string='Base')
    spr_number = fields.Char(string='SPR', copy=False)

    @api.model
    def create(self, vals):
        # import ipdb; ipdb.set_trace()
        if vals.get('project_project_id'):
            project = self.env['project.project'].browse(vals.get('project_project_id')).parent_id
            if not project.sale_sequence_id:
                raise UserError(_('Please set sale order sequence on the project.'))
            date = vals['date_order'].split(' ')[0]
            vals['name'] = project.sale_sequence_id.with_context(ir_sequence_date=date).next_by_id()
    
            # Makes sure partner_invoice_id', 'partner_shipping_id' and 'pricelist_id' are defined
            if any(f not in vals for f in ['partner_invoice_id', 'partner_shipping_id', 'pricelist_id']):
                partner = self.env['res.partner'].browse(vals.get('partner_id'))
                addr = partner.address_get(['delivery', 'invoice'])
                vals['partner_invoice_id'] = vals.setdefault('partner_invoice_id', addr['invoice'])
                vals['partner_shipping_id'] = vals.setdefault('partner_shipping_id', addr['delivery'])
                vals['pricelist_id'] = vals.setdefault('pricelist_id', partner.property_product_pricelist and partner.property_product_pricelist.id)
            result = super(SaleOrder, self).create(vals)
        else:
            result = super(SaleOrder, self).create(vals)
            
        if vals.get('opportunity_id') and self.env['crm.lead'].browse(vals.get('opportunity_id')).commission_id:
            self.env['crm.lead'].browse(vals.get('opportunity_id')).commission_id.commission_sale_id = result.id
        return result
    
    @api.multi
    def action_confirm(self):
        self.ensure_one()
        i = 0
        if sum([1 if line.product_id.type == 'property' else 0 for line in self.order_line]) > 1:
            raise UserError(_('Cannot sale more than 1 stockable product.'))
        for line in self.order_line:
            if line.kpr:
                if line.price_subtotal < self.dp_kpr:
                    raise UserError(_('DP KPR must be less than or equal to unit price.'))
                i += 1
        if self.buyout_term == 'kpr' and i != 1:
            raise UserError(_('Please select one line to pay with kpr.'))
        if any([line.installment <= line.installment_paid for line in self.order_line]):
            raise UserError(_('Please set installment greater than which has been paid.'))
        if not self.project_project_id.parent_id.sequence_id:
            raise UserError(_('Please set sequence on Project.'))
        if not self.spr_number:
            date = self.date_order.split(' ')[0]
            self.spr_number = self.project_project_id.parent_id.sequence_id.with_context(ir_sequence_date=date).next_by_id() 
#             self.spr_number = self.project_project_id.parent_id.sequence_id.next_by_id() 
        return super(SaleOrder, self).action_confirm()
    
    @api.multi
    def action_agreement(self):
        for sale in self:
            sale.state = 'agreement'
    
    @api.multi
    def action_handover(self):
        for sale in self:
            sale.state = 'handover'
    
    @api.multi
    def action_create_installment(self):
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'installment.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': self._context,
        }
    
    @api.multi
    def _create_installment(self, date):
        i = max([line.installment - line.installment_paid for line in self.order_line])
        while i > 0:
            # set date_due
            tgl = eval(date.split('-')[2])
            if tgl > 28:
                raise UserError(_('Please choose date below 29.'))
            month = eval(date.split('-')[1]) + i + line.installment_paid - 1
            if month > 12:
                mod_month = (month % 12)
                year = eval(date.split('-')[0]) + int(month / 12)
                year_str = str(year)
                month = mod_month
            else:
                year_str = date.split('-')[0]
            if len(str(month)) == 1:
                month_str = str(0) + str(month)
            else:
                month_str = str(month)
            assigned_date = year_str + '-' + month_str + '-' + date.split('-')[2]

            lines = []
            for line in self.order_line:
                if i <= line.installment - line.installment_paid:
                    account_id = line.product_id.property_account_income_id.id or \
                        line.product_id.categ_id.property_account_income_categ_id.id
                    line_vals = {
                        'dp': line.kpr,
                        'term': i + line.installment_paid,
                        'product_id': line.product_id.id,
                        'name': line.name + ' - Installment ' + str(i),
                        'account_id': account_id,
                        'quantity': line.product_uom_qty,
                        'price_unit': (line.price_subtotal - line.invoice_paid) / (line.installment - line.installment_paid) \
                            if self.buyout_term == 'cash' else 0,
                        'uom_id': line.product_uom.id,
                        'invoice_line_tax_ids': [tax.id for tax in line.tax_id],
                        'sale_line_ids': [(6, 0, [line.id])]}
                    if self.buyout_term == 'kpr':
                        line_vals['price_unit'] = (self.dp_kpr - line.invoice_paid) / (line.installment - line.installment_paid) \
                            if line.kpr else (line.price_subtotal - line.invoice_paid) / (line.installment - line.installment_paid)
                    if i == max([line.installment - line.installment_paid for line in self.order_line]):
                        line_vals['settlement'] = True
                    lines += [(0, 0, line_vals)]
            
            inv_vals = {
                'partner_id': self.partner_id.id,
                'account_id': self.partner_id.property_account_receivable_id.id,
                'project_id': self.project_project_id.parent_id.id,
                'date_invoice': assigned_date,
                'payment_term_id': self.payment_term_id.id,
                'journal_id': self.env['account.invoice'].default_get(['journal_id'])['journal_id'],
                'company_id': self.env.user.company_id.id,
                'origin': self.name,
                'invoice_line_ids': lines}
            inv_id = self.env['account.invoice'].with_context(project=self.project_project_id.parent_id.id).create(inv_vals)
            i -= 1
        # bank plafond invoice
        if self.buyout_term == 'kpr':
            for line in self.order_line:
                account_id = line.product_id.property_account_income_id.id or \
                    line.product_id.categ_id.property_account_income_categ_id.id
                if line.kpr:
                    line_vals = {
                        'product_id': line.product_id.id,
                        'name': line.name + ' - KPR',
                        'account_id': account_id,
                        'quantity': line.product_uom_qty,
                        'price_unit': line.price_subtotal - self.dp_kpr,
                        'uom_id': line.product_uom.id,
                        'invoice_line_tax_ids': [tax.id for tax in line.tax_id],
                        'sale_line_ids': [(6, 0, [line.id])]}
            lines = [(0, 0, line_vals)]
            
            inv_vals = {
                'partner_id': self.bank_partner_id.id,
                'account_id': self.bank_partner_id.property_account_receivable_id.id,
                'project_id': self.project_project_id.parent_id.id,
                'payment_term_id': self.payment_term_id.id,
                'journal_id': self.env['account.invoice'].default_get(['journal_id'])['journal_id'],
                'company_id': self.env.user.company_id.id,
                'origin': self.name,
                'invoice_line_ids': lines}
            kpr_inv_id = self.env['account.invoice'].with_context(project=self.project_project_id.parent_id.id).create(inv_vals)
        # set state
        self.state = 'installment'
        
    @api.multi
    def print_spr(self):
        return self.env['report'].get_action(self, 'spr')
    
    @api.multi
    def _generate_inv_commission(self):
        if self._context.get('bf'):
            obj = self.env['crm.lead'].browse(self._context.get('active_id'))
            name = 'Commission of booking fee - ' + obj.name
            amount = obj.project_project_id.bf_commission_amount
        else:
            obj = self
            name = 'Commission of installment - ' + obj.name
            amount = obj.commission_amount
        product = obj.company_id.commission_product_id
        if not product:
            raise UserError(_('Please set product in Sale Settings.'))
        
        line_vals = {
            'product_id': product.id,
            'name': name,
            'account_id': product.property_account_expense_id.id,
            'quantity': 1,
            'price_unit': amount,
            'uom_id': product.uom_id.id,
            'account_analytic_id': obj.project_project_id.analytic_account_id.id,
#             'invoice_line_tax_ids': [(6, 0, [tax.id for tax in product.supplier_taxes_id])]
        }
        lines = [(0, 0, line_vals)]
        inv_vals = {
            'type': 'in_invoice',
            'commission': True,
            'commission_sale_id': False if self._context.get('bf') else obj.id,
            'partner_id': obj.user_id.partner_id.id,
            'project_id': self.project_project_id.parent_id.id,
            'account_id': obj.user_id.partner_id.property_account_payable_id.id,
            'journal_id': obj.env['account.invoice'].default_get(['journal_id'])['journal_id'],
            'company_id': obj.company_id.id,
#             'date_invoice': self.name,
            'invoice_line_ids': lines}
        inv = self.env['account.invoice'].with_context(project=self.project_project_id.parent_id.id).create(inv_vals)
        return inv
#         self._cr.execute("UPDATE sale_order SET commission_inv_id=%s WHERE id=%s", (inv.id, self.id))
        
    @api.multi
    def action_view_commission(self):
        invoices = self.mapped('commission_inv_ids')
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoices) > 1:
            action['domain'] = [('id', 'in', invoices.ids)]
        elif len(invoices) == 1:
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoices.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action
        
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    
    @api.depends('product_id.property')
    def _property(self):
        for line in self:
            line.property = True if line.product_id and line.product_id.property else False
            
    installment = fields.Integer(string='Installment', default=1, required=True,
                                 readonly=True, states={'draft': [('readonly', False)]})
    installment_done = fields.Boolean(string='Installment Done', compute='_set_done', store=True)
    invoice_paid = fields.Float(string='Invoices Paid', compute='_get_invoice_paid', store=True)
    installment_paid = fields.Integer(string='Installment Paid', compute='_get_invoice_paid', store=True)
    unit_id = fields.Many2one('sale.unit', string='Unit', copy=False)
    kpr = fields.Boolean(string='KPR', readonly=True, states={'draft': [('readonly', False)]})
    property = fields.Boolean(string='Property', compute='_property', store=True)
#     , compute='_kpr', store=True, readonly=False
    
    @api.onchange('unit_id')
    def onchange_unit_id(self):
        if not self.unit_id:
            return
        name = self.product_id.name
        name += ' ' + self.order_id.project_project_id.parent_id.name if self.order_id.project_project_id.parent_id else ' '
        name += ' ' + self.order_id.project_project_id.name if self.order_id.project_project_id else ' '
        name += ' ' + self.unit_id.block_id.name if self.unit_id.block_id  else ' '
        name += ' ' + self.unit_id.name if self.unit_id  else ' '
        self.name = name
        
    @api.depends('invoice_lines.invoice_id.state')
    def _set_done(self):
        for order_line in self:
            if order_line.invoice_lines:
                if all(inv_line.invoice_id.state == 'paid' for inv_line in order_line.invoice_lines):
                    order_line.installment_done = True
                    # set sale order done
                    if all(line.installment_done for line in order_line.order_id.order_line):
                        self._cr.execute("UPDATE sale_order SET state='settled' WHERE id=%s", (order_line.order_id.id,))
                else:
                    order_line.installment_done = True
                    self._cr.execute("UPDATE sale_order SET state='installment' WHERE id=%s", (order_line.order_id.id,))
            
    @api.depends('invoice_lines.invoice_id.state')
    def _get_invoice_paid(self):
        for order_line in self:
            if order_line.invoice_lines:
                # total of invoice paid on 1 sale order line
                # taken from unit price on invoice line
                amount = sum([inv_line.price_unit if inv_line.invoice_id.state == 'paid' \
                              else 0 for inv_line in order_line.invoice_lines])
                order_line.invoice_paid = amount
                count = sum([1 if inv_line.invoice_id.state == 'paid' \
                              else 0 for inv_line in order_line.invoice_lines])
                order_line.installment_paid = count
                
                # create commission
                paid_lines_amount = sum([line.invoice_paid for line in order_line.order_id.order_line])
                price_lines_amount = sum([line.price_subtotal for line in order_line.order_id.order_line])
                if paid_lines_amount / price_lines_amount >= order_line.order_id.project_project_id.commission_treshhold / 100:
                    order_line.order_id._generate_inv_commission()

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    commission = fields.Boolean(string="Commission")
    commission_sale_id = fields.Many2one('sale.order', string='Sale Order', readonly=True)


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
    
    dp = fields.Boolean(string="DP")
    settlement = fields.Boolean(string="Settlement")
    term = fields.Integer(string="Term")
