{
    "name": "Custom Stock Real Estate/Developer",
    "version": "10.1",
    "depends": [
        'sale_real_estate',
        'stock',
    ],
    "author": "Joenan <joenannr@gmail.com>",
    "category": "",
    "description" : """Custom Stock Real Estate/Developer""",
    'data': [
        'views/stock_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'application' : True,
    'installable' : True,
    'auto_install' : True,
}
