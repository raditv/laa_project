
from odoo import api, fields, models, _
from odoo.tools.float_utils import float_compare
import base64


class AssetReportWizard(models.TransientModel):
    _name = 'asset.report.wizard'
    
    @api.multi
    def export(self):
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'asset.xlsx',
            'nodestroy': True,
            'datas': {
                'date': fields.datetime.now().strftime('%d-%b-%y'),
                'model_ids': self._context.get('active_ids'),
            },
        }