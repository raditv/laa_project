from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class AccountAssetAsset(models.Model):
    _inherit = 'account.asset.asset'
    
    @api.depends('outstanding', 'interest', 'penalty', 'fine', 'deposit')
    def _total_redirection(self):
        self.total_redirection = (self.outstanding + self.interest + self.penalty + self.fine) - self.deposit
    
    @api.depends('auction_cost', 'billing_fee', 'legal_cost', 'renovation_cost')
    def _total_cost(self):
        self.total_cost = self.auction_cost + self.billing_fee + self.legal_cost + self.renovation_cost
    
    @api.depends('total_redirection', 'total_cost')
    def _total_redirect_costs(self):
        self.total_redirect_costs = self.total_redirection + self.total_cost
    
    street = fields.Char('Province id')
    subdistrict_id = fields.Many2one('res.subdistrict', string='Subdistrict')
    province_id = fields.Char('Province id')
    zip = fields.Char(string='ZIP')
    city_id = fields.Many2one('res.city', string='City')
    state_id = fields.Many2one('res.country.state', string='State')
    country_id = fields.Many2one('res.country', string='Country')
    info_ids = fields.One2many('account.asset.info', 'asset_id', string='Info')
    region = fields.Char(string='Region')
    lt = fields.Char(string='LT')
    lb = fields.Char(string='LB')
    guarantee = fields.Char(string='Guarantee')
    
    # Redirection
    outstanding = fields.Monetary(string='Outstanding', digits=0)
    interest = fields.Monetary(string='Tggk Interest', digits=0)
    penalty = fields.Monetary(string='Penalty', digits=0)
    fine = fields.Monetary(string='Fine', digits=0)
    deposit = fields.Monetary(string='Deposit', digits=0)
    total_redirection = fields.Monetary(string='Total Redirection', digits=0, compute='_total_redirection', store=True)
    
    # Additional Costs
    auction_cost = fields.Monetary(string='Auction', digits=0)
    billing_fee = fields.Monetary(string='Billing Fee', digits=0)
    legal_cost = fields.Monetary(string='Legal', digits=0)
    renovation_cost = fields.Monetary(string='Renovation', digits=0)
    total_cost = fields.Monetary(string='Total Costs', digits=0, compute='_total_cost', store=True)
    
    total_redirect_costs = fields.Monetary(string='Total Redirect and Costs', digits=0, compute='_total_redirect_costs', store=True)
    
    # Auction
    auction_date = fields.Date(string='Auction Date')
    auction_amount = fields.Monetary(string='Auction Amount', digits=0)
    
    # Appraisal
    appraisal_date = fields.Date(string='Auction Date')
    market_value = fields.Monetary(string='Market Value', digits=0)
    treshhold_value = fields.Monetary(string='Treshhold Value', digits=0)
    actual_value = fields.Monetary(string='Treshhold Value', digits=0)
    njop_year = fields.Char(string='NJOP Year')
    njop_value = fields.Monetary(string='NJOP Value', digits=0)
    guaranteed = fields.Boolean(string='Guaranteed')
    
    # Sale price
    margin30 = fields.Monetary(string='Margin 30%', digits=0)
    margin40 = fields.Monetary(string='Margin 40%', digits=0)
    margin50 = fields.Monetary(string='Margin 50%', digits=0)
    
    sale_price = fields.Monetary(string='Sale Price', digits=0)
    sale_margin_percent = fields.Float(string='Margin', digits=0)
    sale_margin = fields.Monetary(string='Sale Margin', digits=0)
    
    auction_winner = fields.Char(string='Auction Winner')
    ppjb = fields.Char(string='PPJB')
    
class AccountAssetInfo(models.Model):
    _name = 'account.asset.info'
    _description = 'Asset Info'
    
    asset_id = fields.Many2one('account.asset.asset', string='Asset')
    name = fields.Char(string='Label')
    value = fields.Char(string='Value')
    
