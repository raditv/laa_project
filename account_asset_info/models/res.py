from odoo import models, fields, api
import json
import requests

class ResCity(models.Model):
    _name = "res.city"
    _description = 'City'
    _order = "city_name"
#     _rec_name = "display_name"    

    display_name = fields.Char('Name', compute='_get_display_name', store=True)
    city_id = fields.Char('City Code', size=64, help="The official code for the city")
    name = fields.Char('Name', related='city_name')
    city_name = fields.Char('City Name')
    province = fields.Char('Province')
    province_id = fields.Char('Province id')
    zip = fields.Char('ZIP')
    postal_code = fields.Char('Postal code')
    type = fields.Char('type')
    state_id = fields.Many2one('res.country.state', 'State')
    country_id = fields.Many2one('res.country', 'Country')
    
    @api.multi    
    def get_from_api(self):
        self.ensure_one()
        print('OK')
        # Delete entire data
        city_ids = self.env['res.city'].search([])
        [city.unlink() for city in city_ids]
        
        r = requests.get('http://api.rajaongkir.com/starter/city?key=2a59dafe0fe9b15ee5b03362096321b1')
        data = r.json()
        
        if len(data['rajaongkir']['results']) > 300:
            self.browse([1]).unlink()
            for s in data['rajaongkir']['results']:
                if s['city_name']:
                    c_id = self.env['res.country'].search([('name', '=', 'Indonesia')], limit=1).id
                    s_id = self.env['res.country.state'].search([('name', '=', s["province"])], limit=1).id
                    s.update({'country_id': c_id, 'state_id': s_id})
                    self.create(s)
        return True

    @api.one
    @api.depends(
        'city_id',
        'city_name',
        'province',
        )
    def _get_display_name(self):
        if self.city_name:
            name = [self.city_name]
            if self.province:
                name.append(self.province)
            self.display_name = ", ".join(name)
        else:
            name = ""

    @api.onchange('state_id')
    def onchange_state_id(self):
        if self.state_id:
            self.country_id = self.state_id.country_id
    
class ResSubdistrict(models.Model):
    _name = "res.subdistrict"
    _description = 'Subdistrict'
    _order = "subdistrict_name"
#     _rec_name = "display_name"    

    display_name = fields.Char('Name', compute='_get_display_name', store=True)
    subdistrict_id = fields.Char('Subdistrict Code', size=64, help="The official code for the subdistrict")
    name = fields.Char('Name', related='subdistrict_name')
    subdistrict_name = fields.Char('Subdistrict Name')
    province = fields.Char('Province')
    province_id = fields.Char('Province id')
    zip = fields.Char('ZIP')
    postal_code = fields.Char('Postal code')
    type = fields.Char('Type')
    city_id = fields.Many2one('res.city', 'City')
    state_id = fields.Many2one('res.country.state', 'State')
    country_id = fields.Many2one('res.country', 'Country')
    
    @api.multi    
    def get_from_api(self):
        self.ensure_one()
        print('OK')
        # Delete entire data
        kec_ids = self.env['res.subdistrict'].search([])
        [kec.unlink() for kec in kec_ids]
        city_ids = self.env['res.city'].search([])
        if city_ids:
            for city in city_ids:
#                 import ipdb;ipdb.set_trace();
                r = requests.get('http://pro.rajaongkir.com/api/subdistrict?city=' + str(city.city_id) + '&key=6f4eb9e592e53b42478e6d55b946f3f5')
                data = r.json()
#                 self.browse([1]).unlink()
                for s in data['rajaongkir']['results']:
                    if s['subdistrict_name']:
                        c_id = self.env['res.country'].search([('name', '=', 'Indonesia')], limit=1).id
                        s_id = self.env['res.country.state'].search([('name', '=', s["province"])], limit=1).id
#                         ct_id = self.env['res.city'].search([('city_name', '=', s["city"])], limit=1).id
                        s.update({'country_id': c_id, 'state_id': s_id, 'city_id': city.id})
                        self.create(s)
        return True

    @api.one
    @api.depends(
        'subdistrict_id',
        'subdistrict_name',
        'city_id',
        )
    def _get_display_name(self):
        if self.subdistrict_name:
            name = [self.subdistrict_name]
            if self.city_id:
                display_name = name + [self.city_id.name] + [self.state_id.name]
            self.display_name = ", ".join(display_name)
        else:
            name = ""

    @api.onchange('city_id')
    def onchange_city_id(self):
        if self.city_id:
            self.state_id = self.city_id.state_id
    

