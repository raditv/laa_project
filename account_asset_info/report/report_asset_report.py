import time
from odoo.report import report_sxw
from report_engine_xlsx import report_xlsx
from xlsxwriter.utility import xl_rowcol_to_cell, xl_range

 
class ReportStatus(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        super(ReportStatus, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'cr': cr,
            'uid': uid,
            'time': time,
        })


class report_asset_xlsx(report_xlsx):
 
    def generate_xlsx_report(self, parser, style, data, wb):
        assets = parser.objects.sudo().browse(data['model_ids'])
        
        ws = wb.add_worksheet()
        ws.write('A1', 'LAPORAN NILAI PENGALIHAN ASSET DARI KLG DAN LV', style['title'])
        ws.write('A2', 'UP DATA PER', style['subtitle'])
        ws.write('C2', data['date'], style['subtitle'])
        row, column = 3, 0
        ws.merge_range(xl_range(row, column, row + 2, column), 'NO', style['yellow_center'])
        ws.set_column(row + 1, column, 5); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'TANGGAL PENAGIHAN', style['yellow_center'])
        ws.set_column(row + 1, column, 10); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'NAMA', style['yellow_center'])
        ws.set_column(row + 1, column, 26); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'ALAMAT LENGKAP', style['yellow_center'])
        ws.set_column(row + 1, column, 32); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'WILAYAH', style['yellow_center']); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'LT', style['yellow_center'])
        ws.set_column(row + 1, column, 7); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'LB', style['yellow_center'])
        ws.set_column(row + 1, column, 7); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'JAMINAN', style['yellow_center']); column += 1
        
        ws.merge_range(xl_range(row, column, row, column + 4), 'HISTORY PENGALIHAN ASSET', style['yellow_center'])
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'OUTSTANDING (KLG)/PH (LV)', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'TGGK BUNGA (KLG)/IMBAL HASIL (LV)', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'PINALTY (KLG)/DENDA PINALTY (LV)', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'DENDA (KLG)/DENDA BERJALAN (LV)', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'TITIPAN NASABAH (JIKA ADA)', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'TOTAL PENGALIHAN', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        
        ws.merge_range(xl_range(row, column, row, column + 4), 'BIAYA TAMBAHAN', style['yellow_center'])
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'Lelang', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'Fee Penagihan', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'Legal', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'Renovasi', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'Total Biaya', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'TOTAL HARGA PENGALIHAN + BIAYA TAMBAHAN', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        
        ws.merge_range(xl_range(row, column, row, column + 1), 'LELANG', style['yellow_center'])
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'TGL', style['yellow_center']); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'NILAI', style['yellow_center']); column += 1
        
        ws.merge_range(xl_range(row, column, row, column + 6), 'LAPORAN DATA APPRAISAL ANALIS', style['yellow_center'])
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'TANGGAL', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'NILAI PASAR', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'NILAI WAJAR', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'NILAI TAKSASI', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 1, column + 1), 'NJOP', style['yellow_center'])
        ws.write(row + 2, column, 'TAHUN', style['yellow_center'])
        ws.set_column(row + 2, column, 7); column += 1
        ws.write(row + 2, column, 'NILAI', style['yellow_center'])
        ws.set_column(row + 2, column, 14); column += 1
        ws.merge_range(xl_range(row + 1, column, row + 2, column), 'STATUS JAMINAN', style['yellow_center'])
        ws.set_column(row + 1, column, 10); column += 1
        
        ws.merge_range(xl_range(row, column, row, column + 2), 'REKOMENDASI HARGA JUAL', style['yellow_center'])
        ws.merge_range(xl_range(row + 1, column, row + 1, column + 2), 'MARGIN', style['yellow_center'])
        ws.write(row + 2, column, '30%', style['yellow_center'])
        ws.set_column(row + 2, column, 14); column += 1
        ws.write(row + 2, column, '40%', style['yellow_center'])
        ws.set_column(row + 2, column, 14); column += 1
        ws.write(row + 2, column, '50%', style['yellow_center'])
        ws.set_column(row + 2, column, 14); column += 1
        
        ws.merge_range(xl_range(row, column, row, column + 3), 'KEPUTUSAN HARGA JUAL', style['yellow_center'])
        ws.merge_range(xl_range(row + 1, column, row + 1, column + 1), 'HARGA JUAL', style['yellow_center'])
        ws.merge_range(xl_range(row + 1, column + 2, row + 1, column + 3), 'KETERANGAN', style['yellow_center'])
        ws.write(row + 2, column, 'RUPIAH', style['yellow_center'])
        ws.set_column(row + 2, column, 14); column += 1
        ws.write(row + 2, column, '(%)', style['yellow_center'])
        ws.set_column(row + 2, column, 7); column += 1
        ws.write(row + 2, column, 'UNTUNG (+)', style['yellow_center'])
        ws.set_column(row + 2, column, 14); column += 1
        ws.write(row + 2, column, 'RUGI (-)', style['yellow_center'])
        ws.set_column(row + 2, column, 14); column += 1
        
        ws.merge_range(xl_range(row, column, row + 2, column), 'PEMENANG LELANG', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        ws.merge_range(xl_range(row, column, row + 2, column), 'PPJB', style['yellow_center'])
        ws.set_column(row + 1, column, 14); column += 1
        
        i = 1
        row, column = 6, 0
        for asset in assets:
            ws.write(row, column, i, style['normal']); column += 1
            ws.write(row, column, asset.date, style['normal']); column += 1
            ws.write(row, column, asset.name, style['normal']); column += 1
            address = (asset.street or '') + (asset.subdistrict_id and ' ' + asset.subdistrict_id.name or '') + \
                (asset.city_id and ', ' + asset.city_id.name or '') + (asset.state_id and ', ' + asset.state_id.name or '') + \
                (asset.country_id and ', ' + asset.country_id.name  + ' ' or ' ')  + (asset.zip and ' ' + asset.zip or '')
            ws.write(row, column, address, style['normal']); column += 1
            ws.write(row, column, asset.region or '', style['normal']); column += 1
            ws.write(row, column, asset.lt or '', style['normal']); column += 1
            ws.write(row, column, asset.lb or '', style['normal']); column += 1
            ws.write(row, column, asset.guarantee or '', style['normal']); column += 1
            ws.write(row, column, asset.outstanding, style['normal']); column += 1
            ws.write(row, column, asset.interest, style['normal']); column += 1
            ws.write(row, column, asset.penalty, style['normal']); column += 1
            ws.write(row, column, asset.fine, style['normal']); column += 1
            ws.write(row, column, asset.deposit, style['normal']); column += 1
            ws.write(row, column, asset.total_redirection, style['normal']); column += 1
            
            ws.write(row, column, asset.auction_cost, style['normal']); column += 1
            ws.write(row, column, asset.billing_fee, style['normal']); column += 1
            ws.write(row, column, asset.legal_cost, style['normal']); column += 1
            ws.write(row, column, asset.renovation_cost, style['normal']); column += 1
            ws.write(row, column, asset.total_cost, style['normal']); column += 1
            ws.write(row, column, asset.total_redirect_costs, style['normal']); column += 1
            
            ws.write(row, column, asset.auction_date or '', style['normal']); column += 1
            ws.write(row, column, asset.auction_amount, style['normal']); column += 1
            ws.write(row, column, asset.appraisal_date or '', style['normal']); column += 1
            ws.write(row, column, asset.market_value, style['normal']); column += 1
            ws.write(row, column, asset.treshhold_value, style['normal']); column += 1
            ws.write(row, column, asset.actual_value, style['normal']); column += 1
            ws.write(row, column, asset.njop_year or '', style['normal']); column += 1
            ws.write(row, column, asset.njop_value, style['normal']); column += 1
            ws.write(row, column, 'ISI' if asset.guaranteed else 'KOSONG', style['normal']); column += 1
            
            ws.write(row, column, asset.margin30, style['normal']); column += 1
            ws.write(row, column, asset.margin40, style['normal']); column += 1
            ws.write(row, column, asset.margin50, style['normal']); column += 1
            
            ws.write(row, column, asset.sale_price, style['normal']); column += 1
            ws.write(row, column, asset.sale_margin_percent, style['normal']); column += 1
            ws.write(row, column, asset.sale_margin if asset.sale_margin >= 0 else 0.0, style['normal']); column += 1
            ws.write(row, column, -asset.sale_margin if asset.sale_margin < 0 else 0.0, style['normal']); column += 1
            
            ws.write(row, column, asset.auction_winner or '', style['normal']); column += 1
            ws.write(row, column, asset.ppjb or '', style['normal']); column += 1

            i += 1
            row += 1
            column = 0
            
#             ws.panes_frozen = True
#             ws.remove_splits = True
#             ws.portrait = 0
#             ws.fit_width_to_pages = 1
#             ws.freeze_panes(5, 0)
            
#             column = 'F' if data['format'] == 'detail' and data['type'] == 'Region' else 'E'
#             ws.merge_range("A1:%s1" % column, 'Report Date: %s' % data['date'], style['title'])
#             ws.merge_range("A2:%s2" % column, '%s' % data['type'], style['title'])
#             ws.merge_range("A3:%s3" % column, group_data[1] if data['format'] == 'detail' else group_data[2], style['title'])
#             ws.merge_range("A4:%s4" % column, 'Product Information', style['title'])

#             column = 0
#             if data['format'] == 'detail' and data['type'] == 'Region':
#                 ws.write(4, column, 'Store', style['title'])
#                 ws.set_column(column, column, 15); column += 1
#             ws.write('E8', 'SKU', style['title'])
#             ws.set_column(column, column, 10); column += 1
#             ws.write(4, column, 'Name', style['title'])
#             ws.set_column(column, column, 42); column += 1
#             ws.write(4, column, 'Category', style['title'])
#             ws.set_column(column, column, 35); column += 1
#             ws.write(4, column, 'Qty OH', style['title'])
#             ws.write(4, column + 1, 'Unit', style['title'])
#             ws.set_column(column, column + 1, 10)
#             
#             row = 5
#             cell_s = xl_rowcol_to_cell(row, 3)
#             for line in group_data[0]:
#                 column = 0
#                 if data['format'] == 'detail' and data['type'] == 'Region':
#                     ws.write(row, column, line['store'], style['normal']); column += 1
#                 ws.write(row, column, line['code'], style['normal']); column += 1
#                 ws.write(row, column, line['name'], style['normal']); column += 1
#                 ws.write(row, column, line['category'], style['normal']); column += 1
#                 ws.write(row, column, line['qty'], style['normal'] if line['qty'] >= 0 else style['normal_red']); column += 1
#                 ws.write(row, column, line['uom'], style['normal']);
#                 row += 1
#             
#             ws.merge_range(xl_range(row, 0, row, column - 2), 'Total : ', style['normal_bold_right'])
#             ws.write(row, column - 1, '=SUM(' + cell_s + ':' + xl_rowcol_to_cell(row - 1, column - 1) + ')', style['normal_bold'])
#             ws.write(row, column, '', style['normal'])

 
report_asset_xlsx('report.asset.xlsx', 'account.asset.asset', 'addons/account_asset_info/report/report_excel.mako', parser=ReportStatus, header=False)
