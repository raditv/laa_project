from odoo.report import report_sxw
from odoo import tools
import cStringIO
from odoo.api import Environment
import xlsxwriter

    
class report_xlsx(report_sxw.report_sxw):
 
    def xlsx_style(self, wb):
        return {
            'title' : wb.add_format({'font_size': 20, 'bold': True, 'border': False, 'align': 'left', 'text_wrap' : False}),
            'subtitle' : wb.add_format({'bold': True, 'border': False, 'align': 'left', 'text_wrap' : False}),
            'title_financial' : wb.add_format({'font_color':'#16365c', 'font_size':'28', 'bold': True, 'border': True, 'align': 'center', 'text_wrap' : True}),
            'title_table' : wb.add_format({'bg_color':'#1f497d', 'font_color':'#ffffff', 'bold': True, 'border': True, 'align': 'center', 'text_wrap' : True}),
            'blue' : wb.add_format({'bg_color' : '#ccffff', 'bold': True, 'border': True, 'num_format': '#,##0'}),
            'blue_percent' : wb.add_format({'bg_color' : '#ccffff', 'bold': True, 'border': True, 'num_format': '0.0%'}),
            'blue_center' : wb.add_format({'bg_color': '#ccffff', 'align': 'center', 'bold': True, 'border': True}),
            'yellow' : wb.add_format({'bg_color': '#ffffcc', 'bold': True, 'border': True, 'num_format': '#,##0'}),
            'yellow_center' : wb.add_format({'bg_color': '#ffff00', 'align': 'center', 'valign': 'vcenter', 'bold': True, 'border': True, 'text_wrap' : True}),
            'yellow_right' : wb.add_format({'bg_color': '#ffffcc', 'align': 'right', 'bold': True, 'border': True, 'num_format': '#,##0'}),
            'yellow_percent' : wb.add_format({'bg_color': '#ffffcc', 'bold': True, 'border': True, 'num_format': '0.0%'}),
            'normal' : wb.add_format({'border': True, 'num_format': '#,##0', 'text_wrap' : True}),
            'normal_bold_right' : wb.add_format({'border': True, 'num_format': '#,##0', 'bold': True, 'align': 'right'}),
            'normal_red' : wb.add_format({'border': True, 'num_format': '#,##0', 'bg_color': 'red'}),
            'normal_bold' : wb.add_format({'bold': True, 'border': True, 'num_format': '#,##0'}),
            'normal_date' : wb.add_format({'bold': True}),
            'normal_center' : wb.add_format({'align': 'center', 'border': True}),
            'normal_italic' : wb.add_format({'italic': True, 'border': True}),
            'normal_percent' : wb.add_format({'num_format': '0.0%', 'border': True}),
            'normal_percent_bold' : wb.add_format({'num_format': '0.0%', 'bold': True, 'border': True}),
            'index' : wb.add_format({'num_format': '#', 'align': 'center', 'border': True}),
        }
        
    def create(self, cr, uid, ids, data, context=None):
        uid = 1
        self.env = Environment(cr, uid, context)
        ir_obj = self.env['ir.actions.report.xml']
        report_xml = ir_obj.search([('report_name', '=', self.name[7:])])
        if not report_xml:
            title = ''
            rml = tools.file_open(self.tmpl, subdir=None).read()

            class a(object):

                def __init__(self, *args, **argv):
                    for key, arg in argv.items():
                        setattr(self, key, arg)

            report_xml = a(title=title, report_type='xlsx', report_rml_content=rml, name=title, attachment=False, header=self.header)
        result = self.create_source_xlsx(cr, uid, ids, data, report_xml, context)
        if not result:
            return (False, False)
        return result
 
    def create_source_xlsx(self, cr, uid, ids, data, report_xml, context=None):
        context = {} if not context else context.copy()
        
        rml_parser = self.parser(cr, uid, self.name2, context=context)
        objs = self.getObjects(cr, uid, ids, context=context)
        rml_parser.set_context(objs, data, ids, 'xlsx')
        n = cStringIO.StringIO()
        wb = xlsxwriter.Workbook(n)
        self.generate_xlsx_report(rml_parser, self.xlsx_style(wb), data, wb)
        wb.close()
        n.seek(0)
        return (n.read(), 'xlsx')
 
    def generate_xlsx_report(self, parser, style, data, wb):
        # parser.objects contains records
        raise NotImplementedError()
