from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _

class InstallmentWizard(models.TransientModel):
    _name = 'installment.wizard'
    
    date = fields.Date(string='Invoice Date', required=True)

    def action_confirm(self):
        self.env['sale.order'].browse(self._context.get('params').get('id', self._context.get('active_id'))) and \
            self.env['sale.order'].browse(self._context.get('params').get('id', self._context.get('active_id')))._create_installment(self.date)
        return True
