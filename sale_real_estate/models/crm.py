from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class CrmLead(models.Model):
    _inherit = 'crm.lead'
    _rec_name = 'partner_id'
    
    name = fields.Char('Opportunity', required=False, index=True)
    ktp = fields.Char('KTP', related='partner_id.ktp', readonly=True)
    phone = fields.Char('Phone', related='partner_id.phone', readonly=True)
    unit_id = fields.Many2one('sale.unit', string='Unit', copy=False)
    bf_type = fields.Selection([('deposit', 'Deposit'),
                                ('income', 'Income')], string='Booking Fee Type', required=True,
                               default='income')
    bf_voucher_id = fields.Many2one('account.voucher', string='Booking Fee', readonly=True)
    commission_id = fields.Many2one('account.invoice', string='Commission', readonly=True)
    project_project_id = fields.Many2one('project.project', string='Project', required=True)
    
    @api.multi
    def action_bf_payment(self):
        return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'bf.payment.wizard',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': self._context,
            }
    
    def action_create_bf(self, journal, date, amount, ref):
        voucher_obj = self.env['account.voucher']
        voucher_line_obj = self.env['account.voucher.line']
        crm = self.env['crm.lead'].browse(self._context.get('active_id'))
        
        # get account
        account_id = crm.partner_id.property_account_receivable_id.id if crm.bf_type == 'deposit' \
            else crm.company_id.bf_account_id_setting.id
        if not account_id:
            raise UserError(_('Please set account in Sale Settings as well as in Partner Configuration.'))
        line_vals = {
            'name': 'Booking Fee',
            'account_id': account_id,
            'account_analytic_id': crm.project_project_id.analytic_account_id.id,
            'quantity': 1,
            'price_unit': amount,
        }
        line = [(0, 0, line_vals)]

        vals = {
            'voucher_type': 'sale',
            'partner_id': crm.partner_id.id if crm.bf_type == 'deposit' else False,
            'journal_id': journal.id,
            'account_id': journal.default_debit_account_id.id,
            'pay_now': 'pay_now',
            'date': date,
            'company_id': crm.company_id.id,
            'name': ref,
            # 'state': 'draft',
            'line_ids': line}
        voucher = voucher_obj.create(vals)

        crm.bf_voucher_id = voucher.id
        # validate voucher
        # voucher.proforma_voucher()
        
        # create Commission
        commission = self.env['sale.order'].with_context(bf=True)._generate_inv_commission()
        commission.date_invoice = date
        # set date_due
        month = int(date.split('-')[1]) + 1
        if month > 12:
            mod_month = (month % 12)
            year = int(date.split('-')[0]) + int(month / 12)
            year_str = str(year)
            month = mod_month
        else:
            year_str = date.split('-')[0]
        if len(str(month)) == 1:
            month_str = str(0) + str(month)
        else:
            month_str = str(month)
        commission.date_due = year_str + '-' + month_str + '-' + date.split('-')[2]
        # validate commission
        # commission.action_invoice_open()
        crm.commission_id = commission.id
        
    @api.depends('project_id')
    def _set_commission_sale(self):
        for sale in self:
            if sale.project_id:
                projects = self.env['project.project'].search([('analytic_account_id', '=', sale.project_id.id)])
                if len(projects) > 1:
                    raise UserError(_('It is not allowed more than one analytic account in a project. \
                        For more information please contact administrator'))
                else:
                    sale.project_project_id = projects[0].id


class CrmTeam(models.Model):
    _inherit = 'crm.team'
    
    commission_type = fields.Selection([('percent', 'Percent'),
                                        ('fixed', 'Fixed')], string='Commission Type', required=True)
