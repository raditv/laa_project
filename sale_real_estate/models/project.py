from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class Project(models.Model):
    _inherit = 'project.project'
    
    @api.depends('unit_ids.state', 'unit_ids.progress')
    def _count_unit(self):
        for project in self:
            if not project.child_ids:
                planned = len(project.unit_ids)
                available = sum([1 if unit.state == 'available' else 0 for unit in project.unit_ids])
            else:
                planned = len([child.unit_ids for child in project.child_ids])
                for child in project.child_ids:
                    available = sum([1 if unit.state == 'available' else 0 for unit in child.unit_ids])
            project.update({
                'unit_planned': planned,
                'unit_available': available,
                'unit_booked': planned - available,
            })
            
    @api.depends('child_ids.unit_ids')
    def _get_child_unit_ids(self):
        for project in self:
            if project.child_ids:
                unit_ids = []
                for child in project.child_ids:
                    unit_ids += [unit.id for unit in child.unit_ids]
                project.child_unit_ids = unit_ids
                
    default_bf_type = fields.Selection([('deposit', 'Deposit'),
                                        ('income', 'Income')], string='Default Booking Fee Type', default='income',
                                       required=True)
    bf_commission_amount = fields.Float(string="Booking Fee Commission")
    commission_amount = fields.Float(string="Commission")
    commission_percent = fields.Float(string="Commission (%)")
    commission_treshhold = fields.Float(string="Commission Treshhold (% of total installment)", default=100.0)
    unit_ids = fields.One2many('sale.unit', 'project_id', string='Units')
    block_ids = fields.One2many('sale.unit.block', 'project_id', string='Blocks')
    child_unit_ids = fields.One2many('sale.unit', 'parent_project_id', string='Units', compute='_get_child_unit_ids', store=True)
    unit_planned = fields.Float(string='Planned', compute='_count_unit', store=True)
    unit_available = fields.Float(string='Available', compute='_count_unit', store=True)
    unit_booked = fields.Float(string='Sold out', compute='_count_unit', store=True)
    sale_sequence_id = fields.Many2one('ir.sequence', string='SO Sequence', copy=False)
    invoice_sequence_id = fields.Many2one('ir.sequence', string='Invoice Sequence', copy=False)
    sequence_id = fields.Many2one('ir.sequence', string='SPR Sequence', copy=False)
    
    @api.model
    def create(self, vals):
        res = super(Project, self).create(vals)
        if vals.get('wbs_type') and vals.get('wbs_type') == 'project':
            project = self.env['project.project'].browse(vals.get('parent_id'))
            sequence_id = self.env['ir.sequence'].create({
                'name': vals.get('name'),
                'suffix': '/SPR/' + vals.get('name') + '/SUBANG/%(month)s/%(year)s',
                'padding': 4,
#                 'use_date_range': True,
            })
        return res