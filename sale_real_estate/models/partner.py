from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    bank = fields.Boolean(string='Bank')
    work_phone = fields.Char(string='Work Phone')
    ktp = fields.Char(string='KTP')
    street_ktp = fields.Char()
    street2_ktp = fields.Char()
    zip_ktp = fields.Char(change_default=True)
    city_ktp = fields.Char()
    state_id_ktp = fields.Many2one("res.country.state", string='State', ondelete='restrict')
    country_id_ktp = fields.Many2one('res.country', string='Country', ondelete='restrict')
