from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class SaleUnit(models.Model):
    _name = 'sale.unit'
    _inherit = ['mail.thread']
    _order = 'create_date desc'
    _rec_name = 'display_name'
    
    @api.depends('sale_line_ids.order_id.state', 'lead_ids')
    def _compute_order(self):
        for unit in self:
            if not unit.lead_ids and not unit.sale_line_ids:
                unit.update({'state': 'available',
                             'sale_id': False})
            else:
                if unit.sale_line_ids:
                    unit.sale_id = [line.order_id.id if line.order_id.state != 'cancel' else False for line in unit.sale_line_ids][0]
                unit.state = 'handover' if unit.sale_id.state == 'handover' else 'booked'
    
    @api.depends('name', 'block_id.name')
    def _display_name(self):
        for unit in self:
            unit.display_name = 'Blok ' + unit.block_id.name + ' No. ' + unit.name
                
    name = fields.Char(string='Name', required=True)
    display_name = fields.Char(string='Display Name', compute='_display_name', store=True)
    lead_ids = fields.One2many('crm.lead', 'unit_id', string='Lead')
    sale_line_ids = fields.One2many('sale.order.line', 'unit_id', string='Sale Line')
    sale_id = fields.Many2one('sale.order', string='Sale Order', compute='_compute_order', store=True)
    project_id = fields.Many2one('project.project', string='Project', related='block_id.project_id', store=True, readonly=True)
    company_id = fields.Many2one('res.company', string='Company', related='project_id.company_id', store=True, readonly=True)
    parent_project_id = fields.Many2one('project.project', string='Project')
    block_id = fields.Many2one('sale.unit.block', string='Block', required=False)
    progress = fields.Float(string='Progress (%)')
    state = fields.Selection([('available', 'Available'),
                              ('booked', 'Booked'),
                              ('handover', 'Handover')], compute='_compute_order', string='Status', store=True)
    
    
class SaleUnitBlock(models.Model):
    _name = 'sale.unit.block'
    _inherit = ['mail.thread']
    _order = 'create_date desc'
    
    @api.depends('unit_ids.state')
    def _count_unit(self):
        for block in self:
            planned = len(block.unit_ids)
            available = sum([1 if unit.state == 'available' else 0 for unit in block.unit_ids])
            block.update({
                'unit_planned': planned,
                'unit_available': available,
                'unit_booked': planned - available,
            })

    name = fields.Char(string='Name', required=True)
    project_id = fields.Many2one('project.project', string='Project', required=True)
    company_id = fields.Many2one('res.company', string='Company', related='project_id.company_id', store=True, readonly=True)
    unit_ids = fields.One2many('sale.unit', 'block_id', string='Units')
    unit_planned = fields.Float(string='Planned', compute='_count_unit', store=True)
    unit_available = fields.Float(string='Available', compute='_count_unit', store=True)
    unit_booked = fields.Float(string='Sold out', compute='_count_unit', store=True)

