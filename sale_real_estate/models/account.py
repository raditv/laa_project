from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    project_id = fields.Many2one('project.project', string='Project', domain=[('wbs_type', '=', 'project')])
    
    @api.multi
    def action_invoice_open(self):
        if self.project_id:
            # lots of duplicate calls to action_invoice_open, so we remove those already open
            to_open_invoices = self.filtered(lambda inv: inv.state != 'open')
            if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2', 'draft']):
                raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))
            to_open_invoices.action_date_assign()
            to_open_invoices.with_context(project_id=self.project_id.id).action_move_create()
            return to_open_invoices.invoice_validate()
        else:
            return super(AccountInvoice, self).action_invoice_open()

class AccountMove(models.Model):
    _inherit = 'account.move'
    
    project_id = fields.Many2one('project.project', string='Project', domain=[('wbs_type', '=', 'project')])
    
    @api.multi
    def post(self):
        if self._context.get('project_id'):
            project = self.env['project.project'].browse(self._context.get('project_id'))
            invoice = self._context.get('invoice', False)
            self._post_validate()
            for move in self:
                move.line_ids.create_analytic_lines()
                if move.name == '/':
                    new_name = False
                    journal = move.journal_id
    
                    if invoice and invoice.move_name and invoice.move_name != '/':
                        new_name = invoice.move_name
                    else:
                        if journal.sequence_id:
                            # If invoice is actually refund and journal has a refund_sequence then use that one or use the regular one
                            sequence = journal.sequence_id
                            if invoice and invoice.type in ['out_refund', 'in_refund'] and journal.refund_sequence:
                                if not journal.refund_sequence_id:
                                    raise UserError(_('Please define a sequence for the refunds'))
                                sequence = journal.refund_sequence_id
                            
                            if not project.invoice_sequence_id:
                                raise UserError(_('Please set invoice sequence on the project.'))
                            new_name = project.invoice_sequence_id.with_context(ir_sequence_date=move.date).next_by_id()
                        else:
                            raise UserError(_('Please define a sequence on the journal.'))
    
                    if new_name:
                        move.name = new_name
            return self.write({'state': 'posted'})
        else:
            return super(AccountMove, self).post()
        
    
class AccountVoucher(models.Model):
    _inherit = 'account.voucher'
    
    def _get_written(self):
        Invoice = self.env['account.invoice']
        for this in self:
            if this.currency_id.name == 'IDR':
                written = Invoice.action_write(this.amount, this.currency_id.name, 'id')
            elif this.currency_id.name == 'USD':
                written = Invoice.action_write(this.amount, this.currency_id.name, 'en')
            this.written = written

    written = fields.Char(string='Written', copy=False, compute='_get_written', store=False)