# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class ResCompany(models.Model):
    _inherit = 'res.company'
    
    bf_account_id_setting = fields.Many2one('account.account', 'Booking Fee Income Account',
        help='Default income account used for booking fee')
    commission_product_id = fields.Many2one('product.product', string='Commission Product')
    
class SaleConfiguration(models.TransientModel):
    _inherit = 'sale.config.settings'

    bf_account_id_setting = fields.Many2one('account.account', 'Booking Fee Income Account',
        help='Default income account used for booking fee', related='company_id.bf_account_id_setting')
    default_invoice_policy = fields.Selection(selection_add=[('installment', 'Invoice based on installment')])
    commission_product_id = fields.Many2one('product.product', string='Commission Product',
                                                    related='company_id.commission_product_id')
    
