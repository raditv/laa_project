from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    invoice_policy = fields.Selection(selection_add=[('installment', 'Based on installment')])
    property = fields.Boolean(string='Property')

    @api.onchange('property')
    def onchange_property(self):
        self.type = 'service' if self.property else self.type
            
