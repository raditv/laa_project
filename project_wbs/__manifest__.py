{
    "name": "Project WBS",
    "version": "10.1",
    "depends": [
        'project',
    ],
    "author": "Joenan <joenannr@gmail.com>",
    "category": "",
    "description" : """Project Work Breakdown Structure""",
    'data': [
        'views/project_templates.xml',
        'views/project_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'installable' : True,
    'auto_install' : False,
}
