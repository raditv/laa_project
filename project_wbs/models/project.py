from datetime import date
from datetime import datetime
# from datetime import timedelta
# from dateutil import relativedelta
# import time

from odoo import models, fields, api, _
from openerp.exceptions import UserError
# from openerp.tools.safe_eval import safe_eval as eval
# from openerp.tools.translate import _


class Project(models.Model):
    _inherit = 'project.project'
    
    def _compute_child_count(self):
        for project in self:
            project.child_count = len(project.child_ids)
            
    wbs_type = fields.Selection([('project', 'Project'),
                                 ('phase', 'Phase')], string='WBS Type', default='phase', required=True)
    parent_id = fields.Many2one('project.project', string='Main Project', domain=[('wbs_type', '=', 'project')])
    date_start = fields.Date(string='Start Date')
    date_end = fields.Date(string='End Date')
    progress = fields.Float(string='Progress (%)')
    child_ids = fields.One2many('project.project', 'parent_id', string='Sub Project', readonly=True)
    child_count = fields.Integer(compute='_compute_child_count', string="Child")
    
    project_type = fields.Selection([('property', 'Property'),('nonproperty', 'Non Property')], string='Project Type', default='property')

    @api.multi
    def name_get(self):
        result = []
        for project in self:
            full_name = project.name
            proj_rec = project
            while proj_rec.parent_id:
                full_name = proj_rec.parent_id.name + ' \ ' + full_name
                proj_rec = proj_rec.parent_id
            result.append((project.id, full_name))
        return result
