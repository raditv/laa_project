{
    "name": "Company Logo 2",
    "version": "10.1",
    "depends": [
        'base',
    ],
    "author": "Joenan <joenannr@gmail.com>",
    "category": "",
    "description" : """Company Logo 2""",
    'data': [
        'views/company_view.xml',
    ],
    'demo':[     
    ],
    'test':[
    ],
    'application' : False,
    'installable' : True,
    'auto_install' : False,
}
